class MessageController < ApplicationController
    before_action :set_message, only: [:update, :show, :destroy]
    
    # Tampil Data
    def index
        @message = Message.all        
        render json: {message: "succes", data: @message}, status: :ok
    end

    # Tampil Data :Id
    def show        
        render json: {message: "succes", data: @message}, status: :ok
    end

    # Tambah Data
    def create
        @message = Message.create(message: params[:message], user_id: params[:user_id], room_id: params[:room_id] );
        if @message.valid?
            render json: {message: "success", data: @message}, status: :created
        else
            render json: {message: @message.errors}, status: :unprocessable_entity
        end
    end

    # Edit Data
    def update
        if @message.update(message: params[:message], user_id: params[:user_id], room_id: params[:room_id])
            render json: {message: "success", data: @message}, status: :ok
        else
            render json: {message: @message.errors}, status: :unprocessable_entity
        end
    end

    # Hapus Data
    def destroy
        if @message.destroy
            render json: {message: "success", data: @message}, status: :ok
        else
            render json: {message: @message.errors}, status: :unprocessable_entity
        end
    end

    # Cari Data
    def set_message
        @message = Message.find_by(id: params[:id])
        return render json: { message: "Id Message Not Found" }, status: :not_found if @message.nil?
    end

end
