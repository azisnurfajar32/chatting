Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :users
  get '/confirm/:id', to: 'users#confirm_email'
  post '/auth/login', to: 'authentication#login'
  namespace :api do
    namespace :v1 do
      resources :posts
    end
  end 
  resources :room
  resources :participant
  resources :message
end
