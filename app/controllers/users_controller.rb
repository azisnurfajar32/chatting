class UsersController < ApplicationController
    # before_action :authorize_request, except: %i[create confirm_email index show destroy update]
    # before_action :find_user, only: [:show, :update, :destroy]
  
    # GET /users
    def index
      @users = User.all
      render json: @users
    end
  
    # GET /users/{username}
    def show
      render json: @user, status: :ok
    end
  
    # POST /users
    def create
      @user = User.new(user_params)
      if @user.save
        UserMailer.registration_confirmation(@user).deliver_now
        render json: @user, status: :created
      else
        render json: { errors: @user.errors.full_messages }, status: :unprocessable_entity
      end
    end
  
    # PUT /users/{username}
    def update
      if @user.update(user_params)
        render json: { message: "Updated", data: @user }, status: :ok
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end 
  
    # DELETE /users/{username}
    def destroy
      if @user.destroy
        render json: { message: "success", data: @user }, status: :ok
      else
          render json: { message: @user.errors }, status: :unprocessable_entity
      end      
    end

    def confirm_email
      user = User.find_by_confirm_token(params[:id])
      if user
        user.email_activate
        render json: "Email TerVerifikasi"
      else
        render json: { errors: "Email tidak Terverifikasi" }, status: :bad_request
      end
  end
  
    private
  
    def find_user
      @user = User.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        render json: { errors: 'User not found' }, status: :not_found
    end
  
    def user_params
      params.require(:user).permit(
        :avatar, :name, :username, :email, :password, :password_confirmation
      )
    end
  end