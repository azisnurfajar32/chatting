class RoomController < ApplicationController
    before_action :set_room, only: [:edit, :update, :show, :destroy]
    # Tampil Data
    def index
        @room = Room.all        
        render json: { message: "success", data: @room }, status: :ok
    end

    # Tampil Data/id
    def show        
        render json: { message: "success", data: @room }, status: :ok 
    end

    # Tambah Data
    def create
        @room = Room.create(name: params[:name])
        if @room.valid?
            render json: { message: "success", data: @room }, status: :created
        else
            render json: { message: @room.errors }, status: :unprocessable_entity
        end
    end

    # Update Data
    def update
        if @room.update(name: params[:name])
            render json: { message: "success", data: @room }, status: :ok
        else
            render json: { message: @room.errors }, status: :unprocessable_entity
        end
    end

    # Hapus Data
    def destroy
        if @room.destroy
            render json: { message: "success", data: @room }, status: :ok
        else
            render json: { message: @room.errors }, status: :unprocessable_entity
        end
    end
    def set_room
        @room = Room.find(params[:id])
        return render json: { message: "Id Room Not Found" }, status: :not_found if @room.nil?
    end
end
