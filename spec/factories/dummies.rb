FactoryBot.define do
  factory :dummy do
    name { "MyString" }
    age { rand(1..100) }
    born_at { "2023-03-15 15:49:46" }
    sequence(:email) {|n| "azisnurfajar32+#{n}@gmail.com" }
  end
end
