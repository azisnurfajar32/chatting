class ParticipantController < ApplicationController
    before_action :set_participant, only: [:edit, :update, :show, :destroy]
    
    # Tampil Data
    def index
        @participant = Participant.all
        render json: {message: "succes", data: @participant}, status: :ok
    end
    
    # Tampil Data/id
    def show
        render json: {message: "success", data: @participant}, status: :ok
    end

    # Tambah Data
    def create
        @participant = Participant.create(user_id: params[:user_id], room_id: params[:room_id])
        if @participant.valid?
            render json: {message: "success", data: @participant}, status: :created
        else
            render json: {message: @participant.errors}, status: :unprocessable_entity
        end
    end

    # Edit Data
    def update
        if @participant.update(user_id: params[:user_id], room_id: params[:room_id])
            render json: {message: "success", data: @participant}, status: :ok
        else
            render json: {message: @participant.errors}, status: :unprocessable_entity
        end
    end

    # Hapus Data
    def destroy
        if @participant.destroy
            render json: {message: "success", data: @participant}, status: :ok
        else
            render json: {message: @participant.errors}, status: :unprocessable_entity
        end
    end

    def set_participant
        @participant = Participant.find(params[:id])
        return render json: { message: "Id Participant Not Found" }, status: :not_found if @participant.nil?
    end
end
