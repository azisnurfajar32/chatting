require 'rails_helper'

RSpec.describe "Users", type: :request do
  # Testing Tampil Data User
  describe 'GET /index' do
    before do
      FactoryBot.create_list(:user, 10)
      get '/users'
    end
    it 'returns all posts' do
      expect(json.size).==(10)
    end
    it 'returns status code 200' do
      expect(response).to have_http_status(:success)
    end
  end

  # Testing Tambah Data User
  describe 'POST /create' do
    context 'with valid parameters' do
      let!(:user) { FactoryBot.create(:user) }
      before do
        post '/users', params:{
          user: {
            name: user.name,
            username: user.username,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
          }
        }
      end
      it 'returns the name' do
        expect(json['name'])==(user.name)
      end
      it 'returns the username' do
        expect(json['username'])==(user.username)
      end
      it 'returns the email' do
        expect(json['email'])==(user.email)
      end
      it 'returns the password' do
        expect(json['password'])==(user.password)
      end
      it 'returns the password_confirmation' do
        expect(json['password_confirmation'])==(user.password_confirmation)
      end
      it 'returns a created status' do
        expect(response)==(422)
      end
    end
    context 'with invalid parameters' do
      before do
        post '/users', params:{
          user: {
            name: '',
            username: '',
            email: '',
            password: '',
            password_confirmation: ''
          }
        }
      end
      it 'returns a unprocessable entity status' do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  # Testing Hapus Data
  describe "DELETE /destroy" do
    let!(:user) { FactoryBot.create(:user) }

    before do
      delete "/users/#{user.id}"
    end

    it 'returns status code 204' do
      expect(response).to have_http_status(200)
    end
  end

  # Testing Update Data 
  describe 'PUT /update' do
    context "with valid parameters" do
      let!(:user) {FactoryBot.create(:user)}
      before do 
        put "/users/#{user.id}",params:{
          user: {
            name: user.name,
            username: user.username,
            email: user.email,
            password: user.password,
            password_confirmation: user.password_confirmation
          }
        }
      end
      it 'returns the name' do
        expect(json['name'])==(user.name)
      end
      it 'returns the username' do
        expect(json['username'])==(user.username)
      end
      it 'returns the email' do
        expect(json['email'])==(user.email)
      end
      it 'returns the password' do
        expect(json['password'])==(user.password)
      end
      it 'returns the password_confirmation' do
        expect(json['password_confirmation'])==(user.password_confirmation)
      end
      it 'returns a created status' do
        expect(response)==(422)
      end
    end
    context 'with invalid parameters' do
      let!(:user) {FactoryBot.create(:user)}
      before do
        put "/users/#{user.id}", params:{
          user: {
            name: '',
            username: '',
            email: '',
            password: '',
            password_confirmation: ''
          }
        }
      end
      it 'returns a unprocessable entity status' do
        expect(response).to have_http_status(422)
      end
    end
  end

  # Edit Testing
  # describe 'PUT /update' do
  #   let!(:user) { User.create(name: "auni", phone_number: 620000, email: "auni@gmail.com", password: "12345678") }
  #     scenario 'valid user attributes' do
  #       # send put request to /users/:id
  #       put "/users/#{user.id}", params: {
  #         user: {
  #           name: "auni 123", 
  #           phone_number: 620000123, 
  #           email: "auni_123@gmail.com", 
  #         }
  #       }
    
  #       # response should have HTTP Status 200 OK
  #       expect(response.status).to eq(200)
    
  #       # response should contain JSON of the updated object
  #       json = JSON.parse(response.body).deep_symbolize_keys
  #       expect(json[:name]).to eq("auni 123")
  #       expect(json[:phone_number]).to eq(620000123)
  #       expect(json[:email]).to eq("auni_123@gmail.com")
    
  #       # The user should be updated
  #       expect(user.reload.name).to eq("auni 123")
  #       expect(user.reload.phone_number).to eq(620000123)
  #       expect(user.reload.email).to eq("auni_123@gmail.com")
  #       # expect(user.reload.password).to eq("56788890")
  #     end

  #     scenario 'invalid user attributes' do
  #       # send put request to /users/:id
  #       put "/api/v1/users/#{user.id}", params: {
  #         user: {
  #           name: '', 
  #           phone_number: 123, 
  #           email: 'auni123@gmail.com', 
  #           password: '' 
  #         }
  #       }
    
  #       # response should have HTTP Status 422 Unprocessable entity
  #       expect(response.status).to eq(422)
    
  #       # The user data remain unchanged
  #       expect(user.reload.name).to eq("auni")
  #       expect(user.reload.phone_number).to eq(620000)
  #       expect(user.reload.email).to eq("auni@gmail.com")
  #       expect(user.reload.password).to eq("12345678")
  #     end
  # end

end