class Room < ApplicationRecord
    has_many :messages
    has_many :user, through: :messages

    has_many :participants
    has_many :user, through: :participants
    
    validates :name, presence: true
end
